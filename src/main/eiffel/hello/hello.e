note
	description: "Esempio base per iniziare"
	author: "Mattia Monga"

class
	HELLO

create		-- feature("metodi") da eseguire in fase di creazione dell'oggetto HELLO
	make

feature 	-- sezione di feature: "metodi" della classe HELLO
	answer: INTEGER	-- definizione delle variabili globali

	make
		local		-- definizione delle variabili locali
			s: STRING

		do			-- codice da eseguire nella feature
			s := "Hello"
			answer := 42
			print (s + " World! (con print)%N")
			io.put_string ("The answer is: " + answer.out);
			io.put_new_line

			check	-- verifica che la variabile (globale) soddisfi il predicato
				the_answer_is_right: answer = 43
			end
		end

end
